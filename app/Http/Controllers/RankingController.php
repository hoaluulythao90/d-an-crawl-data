<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\Girl;
use App\Models\Rank;
use Carbon\Carbon;

class RankingController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $time = Carbon::now()->format('Y/m/d h:s');
        $chatLadies = Chat::orderBy('total_count', 'DESC')->get();
        $topGirl = Girl::orderBy('id', 'DESC')->first()->toArray();
        $chartMap = [];
        foreach ($chatLadies as $key => $item) {
            if ($key < 5) {
                $chartMap['left'][$key] = $item;
            } else {
                $chartMap['right'][$key] = $item;
            }
        }

        $rankDay = Rank::where('type', 1)->orderBy('view', 'DESC')->get()->toArray();
        $rankWeek = Rank::where('type', 2)->orderBy('view', 'DESC')->get()->toArray();
        $rankChat = Rank::where('type', 3)->orderBy('view', 'DESC')->limit(3)->get()->toArray();
        $data = [
            'time' => $time,
            'chatLadies' => $chatLadies,
            'topGirl' => $topGirl,
            'left' => !empty($chartMap) ? $chartMap['left'] : null,
            'right' => !empty($chartMap) && isset($chartMap['right']) ? $chartMap['right'] : null,
            'rankDay' => $rankDay,
            'rankWeek' => $rankWeek,
            'rankChat' => $rankChat
        ];
        return view('homepage.home', ['data' => $data]);
    }
}
