<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Girl extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'image',
        'description',
        'url',
        'image_event',
        'create_date',
        'type'
    ];

    protected $table = 'crawl_information';
    public $timestamps = false;
}
