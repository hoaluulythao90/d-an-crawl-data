<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $fillable = [
        'site_name',
        'chat_count',
        'total_count',
        'url',
        'type',
        'create_date'
    ];

    protected $table = 'crawl_chat';
    public $timestamps = false;
}
