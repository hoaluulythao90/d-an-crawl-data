<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" type="text/javascript" href="{{ mix('js/app.js') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>Chat Online</title>
</head>
<body>
<!-- page container -->
<div class="page-container">
<!--    navbar-container-->
    <div class="navbar-container mb-2" id="top">
        <nav class="navbar navbar-expand-lg navbar-light nav-bg row m-0">
            <div class="col-xl-4 col-md-4 col-2 logo-background">
                <div class="d-flex">
                    <a class="navbar-brand home-logo" href="#">
                        <img src="{{ asset('images/head_logo.png') }}" alt="head_logo" class="w-100 h-100">
                    </a>
                </div>
            </div>
            <div class="col-xl-5 col-md-5 col-10 d-flex search-form pr-0">
                <form class="form-inline w-100">
                    <div class="form-group has-search d-flex w-100">
                        <input type="text" class="search-input" id="keyword" placeholder="チャットレディを検索">
                        <span class="fa fa-search form-control-feedback"></span>
                    </div>
                </form>
            </div>
        </nav>
    </div>

<!--    page content-->
    <div class="page-content tab-content">
        <ul class="nav nav-ul">
            <li class="nav-item home">
                <a class="home-text active" data-toggle="tab" href="#home">
                    <span class="home-icon"><i class="fa fa-home"></i></span>
ホーム
                </a>
            </li>
            <li class="nav-item ranking-content">
                <div class="has-border-bottom d-flex home-text second-text">
                    <img src="images/Group.png" alt="icon">
                    <p class="status-text">週間獲得ポイントランキング</p>
                </div>
            </li>
        </ul>
    </div>
    <div class="tab-content pb-5">
        <!--    home container-->
        <div class="tab-pane active" id="home">
            <div class="chat-lady-rank-container mt-4">
                <div class="chat-lady-header">
                    <div class="row">
                        <div class="col-md-3 p-0 mt-0 col-12">
                            <div class="commute-time-week mt-3 text-white ranking-title w-100">【オンライン中】ランキング
                            </div>
                            <div id="carouselId" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    @if (!empty($data['rankChat']))
                                        <div class="carousel-item active w-100 slide-ranking">
                                            <div class="text-center info-girl">
                                                リアルタイム視聴率１位
                                            </div>
                                            <div class="ranking-girl-logo m-0 w-100 position-relative">
                                                <a href="{{ $data['rankChat'][0]['url'] }}" target="_blank" class="w-100 d-block">
                                                    <img src="{{ $data['rankChat'][0]['image'] }}" alt="linhk">
                                                </a>
                                                <div class="sub-inf background w-100">
                                                    <p>パーティ</p>
                                                </div>
                                                <div class="online-inf m-0 w-100">
                                                    <p>{{ $data['rankChat'][0]['view'] }}人視聴中</p>
                                                </div>
                                            </div>
                                            <div class="text-center info-girl">
                                                {{ $data['rankChat'][0]['site_name'] }}
                                                <a href="#" target="_blank">{{ $data['rankChat'][0]['name'] }}</a>
                                            </div>
                                        </div>
                                        @foreach ($data['rankChat'] as $key => $item)
                                            @if ($key > 0)
                                                <div class="carousel-item w-100 slide-ranking">
                                                    <div class="text-center info-girl">
                                                        リアルタイム視聴率{{ ++$key }}位
                                                    </div>
                                                    <div class="ranking-girl-logo m-0 w-100 position-relative">
                                                        <a href="{{ $item['url'] }}" target="_blank" class="w-100 d-block">
                                                            <img src="{{ $item['image'] }}" alt="linhk">
                                                        </a>
                                                        <div class="sub-inf background w-100">
                                                            <p>パーティ</p>
                                                        </div>
                                                        <div class="online-inf m-0 w-100">
                                                            <p>{{ $item['view'] }}人視聴中</p>
                                                        </div>
                                                    </div>
                                                    <div class="text-center info-girl">
                                                        {{ $item['site_name'] }}
                                                        <a href="#" target="_blank">{{ $item['name'] }}</a>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <a class="carousel-control-prev previous-button" href="#carouselId" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next next-button" href="#carouselId" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="info-container mb-2 col-md-9 col-12">
                            <div class="commute-time-week mt-3 text-white">
                                週間獲得ポイントランキング
                            </div>
                            <div class="d-flex w-100">
                                @if (!empty($data['rankWeek']))
                                    @foreach ($data['rankWeek'] as $key => $item)
                                        <div class="ranking-girl-info row mt-1 col-4 w-100 p-0">
                                            <div class="col-xl-5 col-5 p-0 w-100">
                                                <div class="ranking-girl-logo m-0 w-100">
                                                    <a href="{{ $item['url'] }}" target="_blank"><img src="{{ str_replace("',)", '', str_replace("('", '', $item['image'])) }}" alt="linhk"></a>
                                                    <div class="sub-inf background_{{ $item['status'] }} w-100">
                                                        @php
                                                            $status = 'オフライン';
                                                            if ($item['status'] == 1) {
                                                                $status = 'パーティ';
                                                            } else if ($item['status'] == 2) {
                                                                $status = '2ショット';
                                                            }
                                                        @endphp
                                                        <p>{{ $status }}</p>
                                                    </div>
                                                </div>
                                                <div class="online-inf m-0 w-100">
                                                    <p>{{$item['view']}} 人視聴中</p>
                                                </div>
                                            </div>
                                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                                <div class="text-center">
                                                    <img src="images/top.png" alt="logo">
                                                    <span class="ranking-girl-number d-block">0{{ ++$key }}</span>
                                                </div>
                                                <div class="ranking-girl-detail">
                                                    <span class="rank-text">{{ $item['site_name'] }}</span>
                                                    <a href="{{ $item['url'] }}" target="_blank" class="text-red text-decoration-none">
                                                        {{ $item['name'] }}
                                                    </a>
                                                    <div class="girl-detail w-100">
                                                        <span class="detail-text-info over-flow-hidden">{{ $item['description'] }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="commute-time-day pt-2 text-white">日間新人ランキング
                            </div>
                            <div class="d-flex w-100">
                                @if (!empty($data['rankDay']))
                                    @foreach ($data['rankDay'] as $key => $item)
                                        <div class="ranking-girl-info row mt-1 col-4 w-100 p-0">
                                            <div class="col-xl-5 col-5 p-0 w-100">
                                                <div class="ranking-girl-logo m-0 w-100">
                                                    <a href="{{ $item['url'] }}" target="_blank">
                                                        <img src="{{ str_replace("',)", '', str_replace("('", '', $item['image'])) }}" alt="linhk"></a>
                                                    <div class="sub-inf background_{{ $item['status'] }} w-100">
                                                        @php
                                                            $status = 'オフライン';
                                                            if ($item['status'] == 1) {
                                                                $status = 'パーティ';
                                                            } else if ($item['status'] == 2) {
                                                                $status = '2ショット';
                                                            }
                                                        @endphp
                                                        <p>{{ $status }}</p>
                                                    </div>
                                                </div>
                                                <div class="online-inf m-0 w-100">
                                                    <p>{{$item['view']}} 人視聴中</p>
                                                </div>
                                            </div>
                                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                                <div class="text-center">
                                                    <img src="images/top.png" alt="logo">
                                                    <span class="ranking-girl-number d-block">0{{ ++$key }}</span>
                                                </div>
                                                <div class="ranking-girl-detail">
                                                    <span class="rank-text">{{ $item['site_name'] }}</span>
                                                    <a href="{{ $item['url'] }}" target="_blank" class="text-red text-decoration-none">
                                                        {{ $item['name'] }}
                                                    </a>
                                                    <div class="girl-detail w-100">
                                                        <span class="detail-text-info over-flow-hidden">{{ $item['description'] }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="commute-time-chat pt-2 text-white">
                                待機注目度ランキング
                            </div>
                            <div class="d-flex w-100">
                                @if (!empty($data['rankChat']))
                                    @foreach ($data['rankChat'] as $key => $item)
                                        <div class="ranking-girl-info row mt-1 col-4 w-100 p-0">
                                            <div class="col-xl-5 col-5 p-0 w-100">
                                                <div class="ranking-girl-logo m-0 w-100">
                                                    <a href="{{ $item['url'] }}" target="_blank"><img src="{{ str_replace("',)", '', str_replace("('", '', $item['image'])) }}" alt="linhk"></a>
                                                    <div class="sub-inf background_{{ $item['status'] }} w-100">
                                                        @php
                                                            $status = 'オフライン';
                                                            if ($item['status'] == 1) {
                                                                $status = 'パーティ';
                                                            } else if ($item['status'] == 2) {
                                                                $status = '2ショット';
                                                            }
                                                        @endphp
                                                        <p>{{ $status }}</p>
                                                    </div>
                                                </div>
                                                <div class="online-inf m-0 w-100">
                                                    <p>{{$item['view']}} 人視聴中</p>
                                                </div>

                                            </div>
                                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                                <div class="text-center">
                                                    <img src="images/top.png" alt="logo">
                                                    <span class="ranking-girl-number d-block">0{{ ++$key }}</span>
                                                </div>
                                                <div class="ranking-girl-detail">
                                                    <span class="rank-text">{{ $item['site_name'] }}</span>
                                                    <a href="{{ $item['url'] }}" target="_blank" class="text-red text-decoration-none">
                                                        {{ $item['name'] }}
                                                    </a>
                                                    <div class="girl-detail w-100">
                                                        <span class="detail-text-info over-flow-hidden">{{ $item['description'] }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <div class="commute-time-off pt-2 text-white">
                                <img src="{{ asset('images/percent.png') }}" alt="percent" class="d-inline-block percent">
                                週間獲得ポイントランキング
                            </div>
                            <div class="d-flex w-100">
                                @if (!empty($data['rankWeek']))
                                    @foreach ($data['rankWeek'] as $key => $item)
                                        <div class="ranking-girl-info row mt-1 col-4 w-100 p-0">
                                            <div class="col-xl-5 col-5 p-0 w-100">
                                                <div class="ranking-girl-logo m-0 w-100">
                                                    <a href="{{ $item['url'] }}" target="_blank"><img src="{{ str_replace("',)", '', str_replace("('", '', $item['image'])) }}" alt="linhk"></a>
                                                    <div class="sub-inf background_{{ $item['status'] }} w-100">
                                                        @php
                                                            $status = 'オフライン';
                                                            if ($item['status'] == 1) {
                                                                $status = 'パーティ';
                                                            } else if ($item['status'] == 2) {
                                                                $status = '2ショット';
                                                            }
                                                        @endphp
                                                        <p>{{ $status }}</p>
                                                    </div>
                                                </div>
                                                <div class="online-inf m-0 w-100">
                                                    <p>{{$item['view']}} 人視聴中</p>
                                                </div>

                                            </div>
                                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                                <div class="text-center">
                                                    <img src="images/top.png" alt="logo">
                                                    <span class="ranking-girl-number d-block">0{{ ++$key }}</span>
                                                </div>
                                                <div class="ranking-girl-detail">
                                                    <span class="rank-text">{{ $item['site_name'] }}</span>
                                                    <a href="{{ $item['url'] }}" target="_blank" class="text-red text-decoration-none">
                                                        {{ $item['name'] }}
                                                    </a>
                                                    <div class="girl-detail w-100">
                                                        <span class="detail-text-info over-flow-hidden">{{ $item['description'] }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="home-container">
                <div class="commuting-status mt-3">
                    <div class="row">
                        <div class="col-md-12 col-12">
                            <div class="has-border-bottom d-flex">
                                <img src="images/Group.png" alt="icon">
                                <p class="status-text">チャットレディ出勤状況</p>
                            </div>
                            <div class="commute-time mt-3">
                                <span class="pink-text">{{ $data['time'] }}</span>
                                <span class="time-text">更新 ノンアダ・ミックスを対象に掲載</span>
                            </div>
                            <div class="d-flex justify-content-between flex-wrap px-0">
                                <div class="left w-50 pr-1">
                                    @if($data['left'])
                                        @foreach($data['left'] as $key => $chatLady)
                                            <div class="rank-info mt-3 w-100">
                                                <div class="row w-100 ml-0 h-100">
                                                    <div class="col-xl-2 col-md-2 col-2">
                                                        <img src="images/top.png" alt="rank" class="rank-image">
                                                        <span class="rank-number">0{{ $key + 1 }}</span>
                                                    </div>
                                                    <div class="col-xl-8 col-md-7 col-7 mt-3">
                                                        <a href="{{ $chatLady['url'] }}" class="fanza-href" target="_blank">{{ $chatLady['site_name'] }}</a>
                                                        <div class="rank-color">
                                                            @php
                                                                if (!$chatLady['total_count'] || $chatLady['total_count'] == 0) {
                                                                    $chatLady['total_count'] = 1;
                                                                }
                                                                $percent = $chatLady['chat_count'] / $chatLady['total_count'] * 100;
                                                                $green = round($percent, 2);
                                                                $blue = 100 - $green;
                                                            @endphp
                                                            <p class="color-blue" style="width: {{ $blue }}%"></p>
                                                            <p class="color-green" style="width: {{ $green }}%"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-2 col-md-3 col-3 chat-rank-member h-100">
                                                        <p class="pink-text">{{ $chatLady['total_count'] }}人</p>
                                                        <p class="pink-text">{{ $green . '%' }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="right w-50 pl-1">
                                    @if ($data['right'])
                                        @foreach($data['right'] as $key => $chatLady)
                                            <div class="rank-info mt-3 w-100">
                                                <div class="row w-100 ml-0 h-100">
                                                    <div class="col-xl-2 col-md-2 col-2">
                                                        <img src="images/top.png" alt="rank" class="rank-image">
                                                        <span class="rank-number">{{ $key != 9 ? 0 . ($key + 1) : $key + 1 }}</span>
                                                    </div>
                                                    <div class="col-xl-8 col-md-7 col-7 mt-3">
                                                        <a href="{{ $chatLady['url'] }}" class="fanza-href" target="_blank">{{ $chatLady['site_name'] }}</a>
                                                        <div class="rank-color">
                                                            @php
                                                                if (!$chatLady['total_count'] || $chatLady['total_count'] == 0) {
                                                                    $chatLady['total_count'] = 1;
                                                                }
                                                                $percent = $chatLady['chat_count'] / $chatLady['total_count'] * 100;
                                                                $green = round($percent, 2);
                                                                $blue = 100 - $green;
                                                            @endphp
                                                            <p class="color-blue" style="width: {{ $blue }}%"></p>
                                                            <p class="color-green" style="width: {{ $green }}%"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-2 col-md-3 col-3 chat-rank-member h-100">
                                                        <p class="pink-text">{{ $chatLady['total_count'] }}人</p>
                                                        <p class="pink-text">{{ $green . '%' }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="commute-bottom mt-3">
                                <div class="row m-0">
                                    <div class="col-md-3 col-4 d-flex p-0">
                                        <span class="waiting"></span>
                                        <span class="text-base-color ml-2">待機中</span>
                                    </div>
                                    <div class="col-md-3 col-4 d-flex p-0">
                                        <span class="chatting"></span>
                                        <span class="text-base-color ml-2">チャット中</span>
                                    </div>
                                    <div class="col-md-6 col-4 d-flex p-0">
                                        <span class="rating">%</span>
                                        <span class="text-base-color ml-2">チャット率</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="event mt-4">
                    <div class="head_title w-100">
                        <span>《サイト》最新イベント情報</span>
                    </div>
                    <div class="event_content row container mt-2 ml-0">
                        <div class="col-12 col-md-4 event_item">
                            <a href="#" class="mb-2">ライブでゴーゴー</a>
                            <span>開催中 〜 2021-08-31 12:00:00</span>
                            <a href="#" class="description">サマーバケーションポイントギフト</a>
                            <img src="https://livede55.com/img/campaigns/banner/topban_summervacation_pointgift.jpg" alt="">
                        </div>
                        <div class="col-12 col-md-4 event_item">
                            <a href="#" class="mb-2">ライブでゴーゴー</a>
                            <span>開催中 〜 2021-08-31 12:00:00</span>
                            <a href="#" class="description">サマーバケーションポイントギフト</a>
                            <img src="https://livede55.com/img/campaigns/banner/topban_summervacation_pointgift.jpg" alt="">
                        </div>
                        <div class="col-12 col-md-4 event_item">
                            <a href="#" class="mb-2">ライブでゴーゴー</a>
                            <span>開催中 〜 2021-08-31 12:00:00</span>
                            <a href="#"  class="description">サマーバケーションポイントギフト</a>
                            <img src="https://livede55.com/img/campaigns/banner/topban_summervacation_pointgift.jpg" alt="">
                        </div>
                    </div>
                    <div class="event_content row container mt-4 ml-0">
                        <div class="col-12 w-100 text-center mt-5">
                            <a href="#" class="expand w-50 m-auto">チャットレディへの推薦コメントをもっと見る</a>
                        </div>
                    </div>
                </div>
                <div class="event chart mt-4">
                    <div class="head_title w-100">
                        <span>新人ランキング</span>
                    </div>
                    <div class="event_content row container mt-2 ml-0 d-flex">
                        <div class="ranking-girl-info row mt-1 mr-0 col-3 p-0">
                            <div class="col-xl-5 col-5 p-0 w-100">
                                <div class="ranking-girl-logo m-0 w-100">
                                    <a href="#" target="_blank">
                                        <img src="https://pics.dmm.co.jp/livechat/00767399/profile_l.jpg" alt="linhk">
                                    </a>
                                    <div class="sub-inf background_0 w-100">
                                        <p>hello</p>
                                    </div>
                                </div>
                                <div class="online-inf m-0 w-100">
                                    <p>50 人視聴中</p>
                                </div>
                            </div>
                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                <div class="text-center">
                                    <img src="images/top.png" alt="logo">
                                    <span class="ranking-girl-number d-block">0{{ 1 }}</span>
                                </div>
                                <div class="ranking-girl-detail">
                                    <span class="rank-text">Haha</span>
                                    <a href="#" target="_blank" class="text-red text-decoration-none">
                                        Hello
                                    </a>
                                    <div class="girl-detail w-100">
                                        <span class="detail-text-info over-flow-hidden">This is Des</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ranking-girl-info row mt-1 mr-0 col-3 p-0">
                            <div class="col-xl-5 col-5 p-0 w-100">
                                <div class="ranking-girl-logo m-0 w-100">
                                    <a href="#" target="_blank">
                                        <img src="https://pics.dmm.co.jp/livechat/00767399/profile_l.jpg" alt="linhk">
                                    </a>
                                    <div class="sub-inf background_0 w-100">
                                        <p>hello</p>
                                    </div>
                                </div>
                                <div class="online-inf m-0 w-100">
                                    <p>50 人視聴中</p>
                                </div>
                            </div>
                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                <div class="text-center">
                                    <img src="images/top.png" alt="logo">
                                    <span class="ranking-girl-number d-block">0{{ 1 }}</span>
                                </div>
                                <div class="ranking-girl-detail">
                                    <span class="rank-text">Haha</span>
                                    <a href="#" target="_blank" class="text-red text-decoration-none">
                                        Hello
                                    </a>
                                    <div class="girl-detail w-100">
                                        <span class="detail-text-info over-flow-hidden">This is Des</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ranking-girl-info row mt-1 mr-0 col-3 p-0">
                            <div class="col-xl-5 col-5 p-0 w-100">
                                <div class="ranking-girl-logo m-0 w-100">
                                    <a href="#" target="_blank">
                                        <img src="https://pics.dmm.co.jp/livechat/00767399/profile_l.jpg" alt="linhk">
                                    </a>
                                    <div class="sub-inf background_0 w-100">
                                        <p>hello</p>
                                    </div>
                                </div>
                                <div class="online-inf m-0 w-100">
                                    <p>50 人視聴中</p>
                                </div>
                            </div>
                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                <div class="text-center">
                                    <img src="images/top.png" alt="logo">
                                    <span class="ranking-girl-number d-block">0{{ 1 }}</span>
                                </div>
                                <div class="ranking-girl-detail">
                                    <span class="rank-text">Haha</span>
                                    <a href="#" target="_blank" class="text-red text-decoration-none">
                                        Hello
                                    </a>
                                    <div class="girl-detail w-100">
                                        <span class="detail-text-info over-flow-hidden">This is Des</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ranking-girl-info row mt-1 mr-0 col-3 p-0">
                            <div class="col-xl-5 col-5 p-0 w-100">
                                <div class="ranking-girl-logo m-0 w-100">
                                    <a href="#" target="_blank">
                                        <img src="https://pics.dmm.co.jp/livechat/00767399/profile_l.jpg" alt="linhk">
                                    </a>
                                    <div class="sub-inf background_0 w-100">
                                        <p>hello</p>
                                    </div>
                                </div>
                                <div class="online-inf m-0 w-100">
                                    <p>50 人視聴中</p>
                                </div>
                            </div>
                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                <div class="text-center">
                                    <img src="images/top.png" alt="logo">
                                    <span class="ranking-girl-number d-block">0{{ 1 }}</span>
                                </div>
                                <div class="ranking-girl-detail">
                                    <span class="rank-text">Haha</span>
                                    <a href="#" target="_blank" class="text-red text-decoration-none">
                                        Hello
                                    </a>
                                    <div class="girl-detail w-100">
                                        <span class="detail-text-info over-flow-hidden">This is Des</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="event_content row container mt-4 ml-0">
                        <div class="col-12 w-100 text-center mt-5">
                            <a href="#" class="expand w-50 m-auto">チャットレディへの推薦コメントをもっと見る</a>
                        </div>
                    </div>
                </div>
                <div class="event chart mt-4">
                    <div class="head_title w-100">
                        <span>月間アクセス数ランキング</span>
                    </div>
                    <div class="event_content row container mt-2 ml-0 d-flex">
                        <div class="ranking-girl-info row mt-1 mr-0 col-3 p-0">
                            <div class="col-xl-5 col-5 p-0 w-100">
                                <div class="ranking-girl-logo m-0 w-100">
                                    <a href="#" target="_blank">
                                        <img src="https://pics.dmm.co.jp/livechat/00767399/profile_l.jpg" alt="linhk">
                                    </a>
                                    <div class="sub-inf background_0 w-100">
                                        <p>hello</p>
                                    </div>
                                </div>
                                <div class="online-inf m-0 w-100">
                                    <p>50 人視聴中</p>
                                </div>
                            </div>
                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                <div class="text-center">
                                    <img src="images/top.png" alt="logo">
                                    <span class="ranking-girl-number d-block">0{{ 1 }}</span>
                                </div>
                                <div class="ranking-girl-detail">
                                    <span class="rank-text">Haha</span>
                                    <a href="#" target="_blank" class="text-red text-decoration-none">
                                        Hello
                                    </a>
                                    <div class="girl-detail w-100">
                                        <span class="detail-text-info over-flow-hidden">This is Des</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ranking-girl-info row mt-1 mr-0 col-3 p-0">
                            <div class="col-xl-5 col-5 p-0 w-100">
                                <div class="ranking-girl-logo m-0 w-100">
                                    <a href="#" target="_blank">
                                        <img src="https://pics.dmm.co.jp/livechat/00767399/profile_l.jpg" alt="linhk">
                                    </a>
                                    <div class="sub-inf background_0 w-100">
                                        <p>hello</p>
                                    </div>
                                </div>
                                <div class="online-inf m-0 w-100">
                                    <p>50 人視聴中</p>
                                </div>
                            </div>
                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                <div class="text-center">
                                    <img src="images/top.png" alt="logo">
                                    <span class="ranking-girl-number d-block">0{{ 1 }}</span>
                                </div>
                                <div class="ranking-girl-detail">
                                    <span class="rank-text">Haha</span>
                                    <a href="#" target="_blank" class="text-red text-decoration-none">
                                        Hello
                                    </a>
                                    <div class="girl-detail w-100">
                                        <span class="detail-text-info over-flow-hidden">This is Des</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ranking-girl-info row mt-1 mr-0 col-3 p-0">
                            <div class="col-xl-5 col-5 p-0 w-100">
                                <div class="ranking-girl-logo m-0 w-100">
                                    <a href="#" target="_blank">
                                        <img src="https://pics.dmm.co.jp/livechat/00767399/profile_l.jpg" alt="linhk">
                                    </a>
                                    <div class="sub-inf background_0 w-100">
                                        <p>hello</p>
                                    </div>
                                </div>
                                <div class="online-inf m-0 w-100">
                                    <p>50 人視聴中</p>
                                </div>
                            </div>
                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                <div class="text-center">
                                    <img src="images/top.png" alt="logo">
                                    <span class="ranking-girl-number d-block">0{{ 1 }}</span>
                                </div>
                                <div class="ranking-girl-detail">
                                    <span class="rank-text">Haha</span>
                                    <a href="#" target="_blank" class="text-red text-decoration-none">
                                        Hello
                                    </a>
                                    <div class="girl-detail w-100">
                                        <span class="detail-text-info over-flow-hidden">This is Des</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ranking-girl-info row mt-1 mr-0 col-3 p-0">
                            <div class="col-xl-5 col-5 p-0 w-100">
                                <div class="ranking-girl-logo m-0 w-100">
                                    <a href="#" target="_blank">
                                        <img src="https://pics.dmm.co.jp/livechat/00767399/profile_l.jpg" alt="linhk">
                                    </a>
                                    <div class="sub-inf background_0 w-100">
                                        <p>hello</p>
                                    </div>
                                </div>
                                <div class="online-inf m-0 w-100">
                                    <p>50 人視聴中</p>
                                </div>
                            </div>
                            <div class="ranking-girl-icon col-xl-7 col-7 pl-2">
                                <div class="text-center">
                                    <img src="images/top.png" alt="logo">
                                    <span class="ranking-girl-number d-block">0{{ 1 }}</span>
                                </div>
                                <div class="ranking-girl-detail">
                                    <span class="rank-text">Haha</span>
                                    <a href="#" target="_blank" class="text-red text-decoration-none">
                                        Hello
                                    </a>
                                    <div class="girl-detail w-100">
                                        <span class="detail-text-info over-flow-hidden">This is Des</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="event_content row container mt-4 ml-0">
                        <div class="col-12 w-100 text-center mt-5">
                            <a href="#" class="expand w-50 m-auto">月間アクセス数ランキングをもっと見る</a>
                        </div>
                    </div>
                </div>
                <div class="recommend mt-5">
                    <h6 class="text_red recommend_title w-100">RecommendSite[オススメサイト]</h6>
                    <span>編集部が自信を持っておすすめする最優良サイト。まずは無料体験で安心して遊んじゃってください!面白すぎて無駄遣い注意!</span>
                    <div class="recommend_item_list w-100">
                        <div class="big_item row ml-0 d-flex justify-content-between mt-4 w-100">
                            <div class="item">
                                <div class="item_img">
                                    <a href="https://www.livejasmin.com/en/girls/total" target="_blank">
                                        <img src="{{ asset('images/1.jpeg') }}" alt="livejasmin">
                                    </a>
                                </div>
                                <a href="https://www.livejasmin.com/en/girls/total" target="_blank">
                                    LiveJasmin
                                    <i class="fas fa-search"></i>
                                </a>
                            </div>
                            <div class="item">
                                <div class="item_img">
                                    <a href="https://chaturbate.com/" target="_blank">
                                        <img src="{{ asset('images/2.jpeg') }}" alt="livejasmin">
                                    </a>
                                </div>
                                <a href="https://chaturbate.com/" target="_blank">
                                    Chaturbate
                                    <i class="fas fa-search"></i>
                                </a>
                            </div>
                            <div class="item">
                                <div class="item_img">
                                    <a href="https://www.chatpia.jp/main.php" target="_blank">
                                        <img src="{{ asset('images/3.jpeg') }}" alt="livejasmin">
                                    </a>
                                </div>
                                <a href="https://www.chatpia.jp/main.php" target="_blank">
                                    人妻専門CHATPIA
                                    <i class="fas fa-search"></i>
                                </a>
                            </div>
                        </div>
                        <div class="small_item row ml-0 d-flex justify-content-between mt-4 w-100">
                            <div class="item">
                                <div class="item_img">
                                    <a href="https://www.dmm.co.jp/live/chat/" target="_blank">
                                        <img src="{{ asset('images/4.jpeg') }}" alt="livejasmin">
                                    </a>
                                </div>
                                <a href="https://www.dmm.co.jp/live/chat/" target="_blank">
                                    FANZAライブチャット 人妻
                                    <i class="fas fa-search"></i>
                                </a>
                            </div>
                            <div class="item">
                                <div class="item_img">
                                    <a href="https://livede55.com/" target="_blank">
                                        <img src="{{ asset('images/5.jpeg') }}" alt="livejasmin">
                                    </a>
                                </div>
                                <a href="https://livede55.com/" target="_blank">
                                    ライブでゴーゴー
                                    <i class="fas fa-search"></i>
                                </a>
                            </div>
                            <div class="item">
                                <div class="item_img">
                                    <a href="https://bongacams.com/" target="_blank">
                                        <img src="{{ asset('images/6.jpeg') }}" alt="livejasmin">
                                    </a>
                                </div>
                                <a href="https://bongacams.com/" target="_blank">
                                    BongaCams
                                    <i class="fas fa-search"></i>
                                </a>
                            </div>
                            <div class="item">
                                <div class="item_img">
                                    <a href="https://stripchat.com/" target="_blank">
                                        <img src="{{ asset('images/7.jpeg') }}" alt="livejasmin">
                                    </a>
                                </div>
                                <a href="https://stripchat.com/" target="_blank">
                                    StripChat
                                    <i class="fas fa-search"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--    access-->
        </div>

        <!--    ranking-->
        <div class="container tab-pane fade" id="rank">
<!--            ranking-header-->
            <div class="live-chat-rank">
                <p class="live-chat-title">LiveChat Ranking</p>
                <p class="live-chat-detail">ランキングは月でリセットされます。同一人物による連続クリックはカウントされません。</p>
                <div class="chat-lady-rank-container mt-1">
                    <div class="chat-lady-header">
                        <div class="has-border-bottom d-flex">
                            <img src="images/Group.png" alt="icon">
                            <p class="status-text">第1位 ジュエルライブ / 2742人</p>
                        </div>
                        <div class="live-chat-info mt-3">
                            <div class="row">
                                <div class="col-xl-3 col-md-5 col-12 detail-img">
                                    <img src="images/img_1.png" alt="rank" class="live-chat-ona-image">
                                    <div class="d-flex justify-content-between">
                                        <div class="ona-detail">
                                            <a class="see-detail" href="#">詳細情報</a>
                                        </div>
                                        <div class="ona-link">
                                            <a class="see-link" href="#">直リンク</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-10 row col-md-7 col-xl-9">
                                    <div class="live-chat-ona-detail">
                                        <p>女子大生・OL・フリーター・モデル・AV女優など、20代を中心とした若くて可愛い素人の
                                            女の子とエッチなコミュニケーションが楽しめる日本最大級のアダルトライブチャット。業界最安値で双方向チャットが楽し
                                            める独自機能「マルチチャット」を搭載しており、マイクやカメラを使ってお気に入りの女の子とじっくり会話を楽しんだり、
                                            エッチなリクエストを伝えることができます。iOS/Androidにも完全対応しているので、スマートフォンやタブレットでも低料金
                                            で双方向チャットが楽しめます。初回はお試し無料体験付きでフリーメールでも登録OK！初心者の方でも安心してご利用いただけますの
                                            伝えることができます
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container mt-4">
                    <div class="chat-lady-header">
                        <div class="has-border-bottom d-flex">
                            <img src="images/Group.png" alt="icon">
                            <p class="status-text">第2位 ジュエルライブ / 2342人</p>
                        </div>
                        <div class="live-chat-info mt-3">
                            <div class="row">
                                <div class="col-xl-3 col-md-5 col-12 detail-img">
                                    <img src="images/img_1.png" alt="rank" class="live-chat-ona-image">
                                    <div class="d-flex justify-content-between">
                                        <div class="ona-detail">
                                            <a class="see-detail" href="#">詳細情報</a>
                                        </div>
                                        <div class="ona-link">
                                            <a class="see-link" href="#">直リンク</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-10 row col-md-7 col-xl-9">
                                    <div class="live-chat-ona-detail">
                                        <p>女子大生・OL・フリーター・モデル・AV女優など、20代を中心とした若くて可愛い素人の女の子とエッチなコミ
                                            ュニケーションが楽しめる日本最大級のアダルトライブチャット。業界最安値で双方向チャットが楽しめる独自
                                            機能「マルチチャット」を搭載しており、マイクやカメラを使ってお気に入りの女の子とじっくり会話を楽しん
                                            だり、エッチなリクエストを伝えることができます。iOS/Androidにも完全対応しているので、スマートフォン
                                            やタブレットでも低料金で双方向チャットが楽しめます。初回はお試し無料体験付きでフリーメールでも登録OK！
                                            初心者の方でも安心してご利用いただけますので、是非遊んでみて下さいね♪
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container mt-4">
                    <div class="chat-lady-header">
                        <div class="has-border-bottom d-flex">
                            <img src="images/Group.png" alt="icon">
                            <p class="status-text">第3位 ジュエルライブ / 2142人</p>
                        </div>
                        <div class="live-chat-info mt-3">
                            <div class="row">
                                <div class="col-xl-3 col-md-5 col-12 detail-img">
                                    <img src="images/img_1.png" alt="rank" class="live-chat-ona-image">
                                    <div class="d-flex justify-content-between">
                                        <div class="ona-detail">
                                            <a class="see-detail" href="#">詳細情報</a>
                                        </div>
                                        <div class="ona-link">
                                            <a class="see-link" href="#">直リンク</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-10 row col-md-7 col-xl-9">
                                    <div class="live-chat-ona-detail">
                                        <p>女子大生・OL・フリーター・モデル・AV女優など、20代を中心とした若くて可愛い素人の女の子とエッチ
                                            なコミュニケーションが楽しめる日本最大級のアダルトライブチャット。業界最安値で双方向チャットが楽
                                            しめる独自機能「マルチチャット」を搭載しており、マイクやカメラを使ってお気に入りの女の子とじっく
                                            り会話を楽しんだり、エッチなリクエストを伝えることができます。iOS/Androidにも完全対応しているの
                                            で、スマートフォンやタブレットでも低料金で双方向チャットが楽しめます。初回はお試し無料体験付きで
                                            フリーメールでも登録OK！初心者の方でも安心してご利用いただけますので、是非遊んでみて下さいね♪</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>
                <div class="chat-lady-rank-container-for mt-5">
                    <div class="row has-border-bottom">
                        <div class="col-xl-7 col-md-7 col-12 d-flex row">
                            <p class="status-text col-md-3 col-12">第4位</p>
                            <a href="#" class="dxlive col-md-3 col-12">FANZAライブチャット アダルト</a>
                            <div class="ona-detail col-md-3 col-12">
                                <a class="see-detail" href="#">詳細情報</a>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 d-flex row">
                            <p class="status-text col-md-3 col-12">2142人
                            <div class="col-md-5 col-12 rank-star">
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                <i class="fa fa-star text-warning" aria-hidden="true"></i>
                            </div>
                            <p class="status-text col-md-4 col-12">07/24 00:05</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

<!--        campaign-->
        <div class="container tab-pane fade" id="campaign">
            <p class="live-chat-title">Campaign</p>
            <p class="live-chat-detail">キャンペーンをうまく活用してライブチャットを遊びまくれ！要チェック！</p>

<!--            nav tab campaign-->
            <div class="campaign-nav">
                <ul class="nav campaign-item-border" id="myTab" role="tablist">
                    <li class="nav-item campaign-item">
                        <a class="campaign-rank active" id="rank1-tab" data-toggle="tab" href="#rank1" role="tab" aria-controls="home" aria-selected="true">本日開催中 <span class="date">24/07</span></a>
                    </li>
                    <li class="nav-item campaign-item">
                        <a class="campaign-rank" id="rank2-tab" data-toggle="tab" href="#rank2" role="tab" aria-controls="profile" aria-selected="false">明日開催 <span class="date">25/07</span></a>
                    </li>
                    <li class="nav-item campaign-item">
                        <a class="campaign-rank" id="rank3-tab" data-toggle="tab" href="#rank3" role="tab" aria-controls="contact" aria-selected="false">明後日以降開催 <span class="date">26/07</span></a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="rank1" role="tabpanel" aria-labelledby="rank1-tab">
                    <div class="campaign-container mt-1" id="campaignContainer">
                        <div class="chat-lady-header">
                            <div class="has-border-bottom d-flex">
                                <p class="status-text">本日開催中のキャンペーン情報</p>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="rank2" role="tabpanel" aria-labelledby="rank2-tab">
                    <div class="campaign-container mt-1">
                        <div class="chat-lady-header">
                            <div class="has-border-bottom d-flex">
                                <p class="status-text">明日開催予定のキャンペーン情報</p>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="rank3" role="tabpanel" aria-labelledby="rank3-tab">
                    <div class="campaign-container mt-1">
                        <div class="chat-lady-header">
                            <div class="has-border-bottom d-flex">
                                <p class="status-text">明後日以降開催予定のキャンペーン情報</p>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="campaign-info mt-3">
                                <div class="row">
                                    <div class="col-xl-2 col-md-3 col-5 campaign-img">
                                        <img src="images/img_2.png" alt="rank" class="campaign-ona-image">
                                    </div>
                                    <div class="col-xl-5 col-7 col-md-4 campaign-ona-info">
                                        <p class="campaign-date">24/07 (土曜日)</p>
                                        <p class="talk-text">人妻専門CHATPIA</p>
                                        <div class="content-detail">
                                            <div class="campaign-ona-detail">
                                                <a class="see-detail" href="#">詳細情報</a>
                                            </div>
                                            <div class="campaign-ona-link">
                                                <a class="see-link" href="#">直リンク</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-10 col-md-5 osusume-ona">
                                        <div class="info-scroll scroll-item">
                                            <div class="d-flex">
                                                <p class="osusume-text">オススメ度:</p>
                                                <div>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                    <i class="fa fa-star text-warning" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <p class="osusume-title">Madam Kan Beer Garden</p>
                                            <p class="osusume-detail">【7月末までの長期開催!】<br>
                                                期間  7月12日(月)0:00～7月31日(土)23:59<br>
                                                内容  期間内に初めてチャットする女性との2ショットが20%OFF!!<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="footer">
        <div class="footer__top container">
            <div class="top-scroll">
                <div class="wrap">
                    <div class="row no-gutter">
                        <div class="col-lg-6 offset-lg-6 text-right">
                            <a href="#top" class="button">
                                <p class="pagetop">PAGE TOP</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-menu">
                <div class="wrap">
                    <div class="row no-gutter">
                        <div class="col-md-4">
                            <div class="footer-group" id="group_ranking">
                                <dt>ランキング</dt>
                            </div>
                            <div class="footer-nav nav-hidden" id="ranking">
                                <a href="/pc/ranking/top.php">獲得ポイントランキング</a>
                                <a href="/pc/ranking/newregist.php">新人ランキング</a>
                                <a href="/pc/ranking/favorite.php">お気に入り登録数ランキング</a>
                                <a href="/pc/ranking/twoshot.php">２ショット時間ランキング </a>
                                <a href="/pc/ranking/logintime.php">オンライン時間ランキング</a>
                                <a href="/pc/ranking/waiting.php">待機注目度ランキング</a>
                                <a href="/pc/ranking/viewer.php">リアルタイム視聴率</a>
                                <a href="/pc/ranking/pv.php">月間アクセス数ランキング</a>
                                <a href="/pc/ranking/vote.php">月間かわいいね数ランキン</a>
                                <a href="/pc/ranking/newhalf.php">新人半額中</a>
                                <a href="/pc/site/graph.php">《サイト》リアルタイムランキング</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="footer-group" id="group_chat">
                                <dt>サイト一覧</dt>
                            </div>
                            <div class="footer-nav nav-hidden" id="chat">
                                <a href="/pc/site/dmm-acha.html">FANZAアダルト</a>
                                <a href="/pc/site/dmm-ocha.html">FANZAノンアダルト</a>
                                <a href="/pc/site/dmm-macha.html">FANZA人妻</a>
                                <a href="/pc/site/dmm-virtual.html">FANZAバーチャル</a>
                                <a href="/pc/site/angel-live.html">エンジェルライブ</a>
                                <a href="/pc/site/chatpia.html">CHATPIA</a>
                                <a href="/pc/site/livede55.html">ライブデゴーゴー</a>
                                <a href="/pc/site/dxlive.html">DXLIVE</a>
                                <a href="/pc/site/j-live.html">ジュエルライブ</a>
                                <a href="/pc/site/madamlive.html">マダムライブ</a>
                                <a href="/pc/site/bbchat.html">BBチャット </a>
                                <a href="/pc/site/madamu.html">マダムとおしゃべり館 </a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-group" id="group_tool">
                                <dt>ツール</dt>
                            </div>
                            <div class="footer-nav nav-hidden" id="tool">
                                <a href="/pc/tools/onlinechecker.php">ログイン通知（オンラインチェッカー）</a>
                                <a href="/pc/tools/mypage.php">登録情報</a>
                                <a href="/pc/tools/resetpassword.php">パスワード再設定</a>
                                <a href="/pc/tools/withdrawal.php">ユーザー登録の解除</a>
                                <a href="/pc/performer/search.php">検索</a>
                                <a href="/pc/performer/comment.php">推薦コメント</a>

                                <a href="/pc/site/event.php">《サイト》最新イベント情報</a>
                                <a href="/pc/site/comparison.php">《サイト》ライブチャット比較</a>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer-group" id="group_content_menu">
                                <dt>コンテンツ</dt>
                            </div>
                            <div class="footer-nav nav-hidden" id="content_menu">
                                <a href="/pc/blog/">Chatzone編集部
                                </a>
                                <a href="/pc/contents/">ライブチャットとは</a>
                                <a href="/pc/tools/getapp.php">無料ポイント</a>
                            </div>


                            <br> <br> <br>

                            <div class="footer-group" id="group_others">
                                <dt>その他</dt>
                            </div>
                            <div class="footer-nav nav-hidden" id="others">
                                <a href="/pc/welcome.php">ようこそ</a>
                                <a href="/pc/contents/privacy-policy">プライバシーポリシー</a>
                                <a href="/pc/news.php">お知らせ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__copyright">
            <div class="wrap container">
                    <div class="row">
                        <div class="col-6">
                            <p>©2011～2021　チャットゾーン</p>
                        </div>
                        <div class="col-6">
                            <div class="float-right">
                                <a href="https://www.chatzone.jp/">HOME | </a>
                                <a href="/pc/contents/vip.php">VIP会員制度</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
