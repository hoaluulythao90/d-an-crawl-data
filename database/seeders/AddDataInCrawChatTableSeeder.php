<?php

namespace Database\Seeders;

use App\Models\Chat;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Table;

class AddDataInCrawChatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            0 => [
                'site_name' => 'FANZAライブチャット 人妻',
                'chat_count' => 97,
                'total_count' => 51.55,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 1,
                'create_date' => Carbon::now()
            ],
            1 => [
                'site_name' => 'FANZAライブチャット ノンアダルト',
                'chat_count' => 88,
                'total_count' => 65,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 1,
                'create_date' => Carbon::now()
            ],
            2 => [
                'site_name' => '人妻専門CHATPIA',
                'chat_count' => 86,
                'total_count' => 55,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 1,
                'create_date' => Carbon::now()
            ],
            3 => [
                'site_name' => '人妻専門CHATPIA',
                'chat_count' => 12,
                'total_count' => 22,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 1,
                'create_date' => Carbon::now()
            ],
            4 => [
                'site_name' => 'マダムライブ',
                'chat_count' => 97,
                'total_count' => 51.55,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 1,
                'create_date' => Carbon::now()
            ],
            5 => [
                'site_name' => 'FANZAライブチャット 人妻',
                'chat_count' => 127,
                'total_count' => 51.55,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 2,
                'create_date' => Carbon::now()
            ],
            6 => [
                'site_name' => 'FANZAライブチャット ノンアダルト',
                'chat_count' => 199,
                'total_count' => 65,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 2,
                'create_date' => Carbon::now()
            ],
            7 => [
                'site_name' => '人妻専門CHATPIA',
                'chat_count' => 22,
                'total_count' => 55,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 2,
                'create_date' => Carbon::now()
            ],
            8 => [
                'site_name' => '人妻専門CHATPIA',
                'chat_count' => 112,
                'total_count' => 22,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 2,
                'create_date' => Carbon::now()
            ],
            9 => [
                'site_name' => 'マダムライブ',
                'chat_count' => 47,
                'total_count' => 51.55,
                'url' => 'http://www.chatlife.jp/link/link.php?Code=macha',
                'type' => 2,
                'create_date' => Carbon::now()
            ],
        ];

        foreach ($data as $value)
            Chat::create($value);
    }
}
